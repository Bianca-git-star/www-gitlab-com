---
layout: handbook-page-toc
title: "GitLab Access Manager"
description: "The IT Self Service handbook page provides all of our team members easy access to all of the processes and solutions for IT related services."
---

## On this page
{:.no_toc}

- TOC
{:toc}

GitLab Access Manager is a custom built full stack application built by the GitLab IT Engineering team that provides a user interface ("UI") for team members, managers, access approvers, audit reviewers, and IT administrators to centrally approve and manage role-based access to the directory of [tech stack applications](/handbook/business-technology/tech-stack-applications/) ("SaaS providers").

In FY21-Q4, we launched the [GitLab Sandbox Cloud](/handbook/infrastructure-standards/realms/sandbox/), powered by [HackyStack](https://gitlab.com/hackystack/hackystack-portal) to automate the provisioning of AWS acccounts, AWS IAM users, GCP projects, and GCP users. This has allowed us to automate a large portion of our AWS and GCP access requests.

In FY22-Q3, we launched the initial technical discovery and custom development prototype of GitLab Access Manager (codename "Project FastPass") that will replace access request issues with progressive milestones throughout FY23. All remaining manual provisioning will include a streamlined custom web UI and API integration with all of our tech stack applications for user and role provisioning.

## Reference Links

* **DRI:** [Jeff Martin](/company/team/#jeffersonmartin), [Dillon Wheeler](/company/team/#dillonwheeler), [Peter Kaldis](/company/team/#pkaldis)
* **Stable Counterparts:** [Jeff Burrows (Manager, Security Compliance)](/company/team/#jburrows001), [Derek Isla (Sr. IT Compliance Manager)](/company/team/#disla)
* `#gitlab-access-manager` Slack Channel
* [Handbook Page](/handbook/business-technology/engineering/access-manager)
* [Epics](https://gitlab.com/gitlab-com/business-technology/engineering/access-manager/-/epics)
* [Issue Tracker](https://gitlab.com/gitlab-com/business-technology/engineering/access-manager/-/issues)
* [Access Manager Docs (Preview)](https://docs.access.gitlabenvironment.cloud) (internal)
* [Access Manager Docs (Code)](https://gitlab.com/gitlab-com/business-technology/engineering/access-manager/gitlab-access-manager-docs) (internal)
* [Access Manager Application Code](https://gitlab.com/gitlab-com/business-technology/engineering/access-manager/gitlab-access-manager-app) (internal)
* [YouTube Playlist - Weekly Development Demos](https://www.youtube.com/playlist?list=PL05JrBw4t0KoLbqn20qVAX8f-ZGvbb88V)
* [Google Drive Folder with Slide Decks](https://drive.google.com/drive/folders/1qY4KCTAM26VEmUPPcKFS8EdK1p3McxU_)
* [Development Roadmap](https://drive.google.com/drive/folders/1W1861aFWo8XBoBYbI95FbRX6zDS2bsAr)

### Current State

You can track the real-time progress in [GitLab Access Manager](https://gitlab.com/groups/gitlab-com/business-technology/engineering/access-manager) [epics](https://gitlab.com/groups/gitlab-com/business-technology/engineering/access-manager/-/epics) and [issues](https://gitlab.com/gitlab-com/business-technology/engineering/access-manager/-/issues).

The GitLab Access Manager documentation draft is available at [https://docs.access.gitlabenvironment.cloud](https://docs.access.gitlabenvironment.cloud) for internal education and security compliance review. 

The application is in the early stages of design and development, however team members can view a WIP preview at [https://glam-dev.gitlab.systems](https://glam-dev.gitlab.systems).

### Future State

Access Manager has back-end automation that uses the API for each SaaS provider to automate user account and role provisioning (after approval) and has scheduled deprovisioning of user accounts based on expiration or offboarding date.

There are several additional features for streamlining access/audit reviews and compliance reporting using the UI, API, or CSV exports.

In other words, the functionality of the application focuses on the automation and auditability of the lifecycle of Identity and Access Management ("IAM") and Role Based Access Control ("RBAC") for team members and our tech stack applications.

It is important to distinguish that Access Manager automates the provisioning process for SaaS Provider systems behind the scenes, and users still use Okta as our single sign-on identity provider. For SaaS Providers that do not support Okta authentication, Access Manager uses the API to provision a local authentication username and password that is automatically deprovisioned when the team member access expires or is offboarded.
